package DMadera.BSN;

import DMadera.DAO.Impl.clienteDAOList;
import DMadera.DAO.Impl.pedidoDAOList;
import DMadera.DAO.clienteDAO;
import DMadera.DAO.pedidoDAO;
import DMadera.Modelos.Pedido;

import java.util.ArrayList;
import java.util.List;

public class pedidoBSN {
    private clienteDAO clientedao;
    private pedidoDAO pedidodao;
    private ArrayList<String> estados = new ArrayList<String>();

    public pedidoBSN(){
        this.clientedao = new clienteDAOList();
        this.pedidodao = new pedidoDAOList();
    }

    public List<String> estadosDisponibles() {
        estados.add("Adquiriedo materiales");
        estados.add("Obra en proceso");
        estados.add("Pintado en proceso");
        estados.add("Detalles finales");
        estados.add("Producto terminado");
        return estados;
    }

    public List<Pedido> listarPedido() {
        return this.pedidodao.listarPedido();
    }

    public void actualizarPedido(Pedido pedido, String estado, String novedad, String descripcion) {
        pedidodao.actualizarPedido(pedido,estado,novedad,descripcion);
    }

    public List<Pedido> consultarPedidos(String id) {
        return this.pedidodao.consultarPedidos(id);
    }
}
