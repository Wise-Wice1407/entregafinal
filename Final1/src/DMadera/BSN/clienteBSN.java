package DMadera.BSN;

import DMadera.DAO.Impl.clienteDAOList;
import DMadera.DAO.Impl.pedidoDAOList;
import DMadera.DAO.clienteDAO;
import DMadera.DAO.pedidoDAO;
import DMadera.Modelos.Cliente;
import DMadera.Modelos.Pedido;

import java.util.List;

public class clienteBSN {
    private clienteDAO clientedao;
    private pedidoDAO pedidodao;

    public clienteBSN(){
        this.clientedao = new clienteDAOList();
        this.pedidodao = new pedidoDAOList();
    }

    public void registrarCLiente(Cliente cliente) {
        this.clientedao.registrarCliente(cliente);

    }

    public void registrarPedido(Pedido pedido, Cliente cliente) {
        this.pedidodao.registrarPedido(pedido);
        this.clientedao.agregarProducto(cliente);
    }

    public List<Cliente> listarCliente() {
        return this.clientedao.listarCliente();
    }

    public void borrarCliente(Cliente cliente) {
        this.clientedao.borrarCliente(cliente);
    }

    public void actualizarCliente(Cliente cliente, String nombre, String id, String contacto) {
        this.clientedao.actualizarCliente(cliente,nombre,id,contacto);
    }
}
