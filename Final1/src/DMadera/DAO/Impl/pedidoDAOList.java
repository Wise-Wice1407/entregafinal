package DMadera.DAO.Impl;

import DMadera.DAO.pedidoDAO;
import DMadera.Modelos.Pedido;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


public class pedidoDAOList implements pedidoDAO {
    private static List<Pedido> bd = new ArrayList<>();

     @Override
    public void registrarPedido(Pedido pedido) {
         pedido.setEstado("Procesando");
         bd.add(pedido);
    }

    @Override
    public List<Pedido> listarPedido() {
        return new ArrayList<>(bd);
    }

    @Override
    public void actualizarPedido(Pedido pedido, String estado, String novedad, String descripcion) {
        pedido.setEstado(estado);
        pedido.setNovedad(novedad);
        pedido.setDescripcionNovedad(descripcion);
    }

    @Override
    public void borrarPedido(Pedido pedido) {
        bd.remove(pedido);
    }

    @Override
    public List<Pedido> consultarPedidos(String id) {
        return bd.stream()
                .filter(pedido-> pedido.getIdcliente().equals(id))
                .collect(Collectors.toList());
    }
}
