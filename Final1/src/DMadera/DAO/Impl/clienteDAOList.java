package DMadera.DAO.Impl;

import DMadera.DAO.clienteDAO;
import DMadera.Modelos.Cliente;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class clienteDAOList implements clienteDAO {
    private static List<Cliente> bd = new ArrayList<>();

    @Override
    public void registrarCliente(Cliente cliente) {
        bd.add(cliente);
    }

    @Override
    public Optional<Cliente> consultarCliente(String id) {
        return bd.stream()
                .filter(cliente -> cliente.getId().equals(id))
                .findFirst();
    }

    @Override
    public List<Cliente> listarCliente() {
        return new ArrayList<>(bd);
    }

    @Override
    public void borrarCliente(Cliente cliente) {
        bd.remove(cliente);
    }

    @Override
    public void actualizarCliente(Cliente cliente, String nombre, String id, String contacto) {
        cliente.setNombre(nombre);
        cliente.setId(id);
        cliente.setContacto(contacto);
    }

    @Override
    public void agregarProducto(Cliente cliente) {
        cliente.agregarPedido();
    }
}
