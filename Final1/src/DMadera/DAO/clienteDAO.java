package DMadera.DAO;

import DMadera.Modelos.Cliente;

import java.util.List;
import java.util.Optional;

public interface clienteDAO {

    void registrarCliente(Cliente cliente);

    Optional<Cliente> consultarCliente(String id);

    List<Cliente> listarCliente();

    void borrarCliente(Cliente clienteElegido);

    void actualizarCliente(Cliente cliente, String nombre, String id, String contacto);

    void agregarProducto(Cliente cliente);
}
