package DMadera.Controller;

import DMadera.BSN.pedidoBSN;
import DMadera.Modelos.Pedido;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

import java.util.List;


public class actualizarPedidoController {
    @FXML
    private TextField txtnovedad;
    @FXML
    private TextField txtdescripcionNovedad;
    @FXML
    private ComboBox cmbestados;
    @FXML
    private ComboBox cmbproductos;

    private pedidoBSN pedidobsn = new pedidoBSN();


    @FXML
    private void initialize(){
        List<String> estadosExistentes = pedidobsn.estadosDisponibles();
        ObservableList<String> estados = FXCollections.observableList(estadosExistentes);
        cmbestados.setItems(estados);

        List<Pedido> pedidosExistentes = pedidobsn.listarPedido();
        ObservableList<Pedido> pedidos = FXCollections.observableList(pedidosExistentes);
        cmbproductos.setItems(pedidos);
    }

    public void btnguardarestado_Action(){
        Pedido pedidoElegido = (Pedido) cmbproductos.getValue();
        if(pedidoElegido == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Actualizacion de pedido");
            alert.setHeaderText("Ha ocurrido un error");
            alert.setContentText("Seleccione un producto");
            alert.showAndWait();
            return;
        }
        String estadoIngresado = (String)cmbestados.getValue();
        if (estadoIngresado == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Actualizacion de pedido");
            alert.setHeaderText("Ha ocurrido un error");
            alert.setContentText("Seleccione un estado");
            alert.showAndWait();
            return;
        }
        String descripcionIngresada = txtdescripcionNovedad.getText();
        String novedadIngresada = txtnovedad.getText();
        if(txtdescripcionNovedad.getText() == null){
           descripcionIngresada  = "   ";
        }

        boolean esValido = validarCampos(novedadIngresada);
        if (!esValido) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Actualizacion de pedido");
            alert.setHeaderText("Ha ocurrido un error");
            alert.setContentText("Diligencie el nombre o escriba ninguna");
            alert.showAndWait();
            return;
        }

        pedidobsn.actualizarPedido(pedidoElegido,estadoIngresado,novedadIngresada,descripcionIngresada);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Actualizacion de pedido");
        alert.setHeaderText("Resultado de la operación");
        alert.setContentText("El registro ha sido exitoso");
        alert.showAndWait();
        limpiarCampos();
    }

    private boolean validarCampos(String... campos) {
        for (int i = 0; i < campos.length; i++) {
            if (campos[i] == null || "".equals(campos[i])) {
                return false;
            }
        }
        return true;
    }

    public void limpiarCampos(){
        txtnovedad.clear();
        txtdescripcionNovedad.clear();
    }
}
