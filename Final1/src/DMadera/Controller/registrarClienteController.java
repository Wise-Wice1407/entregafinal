package DMadera.Controller;

import DMadera.BSN.clienteBSN;
import DMadera.Modelos.Cliente;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;


public class registrarClienteController {
    @FXML
    private TextField txtnombres;
    @FXML
    private TextField txtid;
    @FXML
    private TextField txtcontacto;

    private clienteBSN clientebsn = new clienteBSN();

    public void btnconfirmar_Action(){
        String nombreIngresado = txtnombres.getText().trim();
        String idIngresado = txtid.getText().trim();
        String contactoIngresado = txtcontacto.getText().trim();

        boolean esValido = validarCampos(nombreIngresado,idIngresado,contactoIngresado);

        if(!esValido){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de cliente");
            alert.setHeaderText("Ha ocurrido un error");
            alert.setContentText("Diligencie todos los campos");
            alert.showAndWait();
            return;
        }

        Cliente cliente = new Cliente(nombreIngresado,idIngresado,contactoIngresado);
        clientebsn.registrarCLiente(cliente);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Registro de cliente");
        alert.setHeaderText("Resultado de la operación");
        alert.setContentText("El registro ha sido exitoso");
        alert.showAndWait();
        limpiarCampos();
    }

    private boolean validarCampos(String... campos) {
        for (int i = 0; i < campos.length; i++) {
            if (campos[i] == null || "".equals(campos[i])) {
                return false;
            }
        }
        return true;
    }

    private void limpiarCampos(){
        txtid.clear();
        txtnombres.clear();
        txtcontacto.clear();
    }
}
