package DMadera.Controller;

import DMadera.BSN.clienteBSN;
import DMadera.Modelos.Cliente;
import DMadera.Modelos.Pedido;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.util.List;

public class registrarPedidoController {
    @FXML
    private ComboBox<Cliente> cmbcliente;
    @FXML
    private Label lblnumeropedido;
    @FXML
    private TextField txtdescripcionpedido;

    private clienteBSN clientebsn = new clienteBSN();

    @FXML
    private void initialize(){
        List<Cliente> cliente = clientebsn.listarCliente();
        ObservableList<Cliente> clientesExistentes = FXCollections.observableList(cliente);
        cmbcliente.setItems(clientesExistentes);
    }

    public void cmbcliente_Action(){
        Cliente clienteElegido = cmbcliente.getValue();
        if(clienteElegido == null){
            return;
        }
        lblnumeropedido.setText("N°"+clienteElegido.getId()+clienteElegido.pedidosCliente());

    }

    public void btnguardar_Action(){
        Cliente clienteElegido = cmbcliente.getValue();

        if (clienteElegido == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de pedido");
            alert.setHeaderText("Ha ocurrido un error");
            alert.setContentText("Seleccione un cliente");
            alert.showAndWait();
            return;
        }

        String descripcioningresada = txtdescripcionpedido.getText().trim();
        String numeroobtenido = lblnumeropedido.getText();
        boolean esValido = validarCampos(descripcioningresada,numeroobtenido);

        if (!esValido) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de pedido");
            alert.setHeaderText("Ha ocurrido un error");
            alert.setContentText("Diligencie todos los campos");
            alert.showAndWait();
            return;
        }

        Pedido pedido = new Pedido(numeroobtenido,descripcioningresada);
        pedido.setCliente(clienteElegido);
        pedido.setIdcliente(clienteElegido.getId());
        this.clientebsn.registrarPedido(pedido,clienteElegido);
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Registro de pedido");
        alert.setHeaderText("Resultado de la operación");
        alert.setContentText("El registro ha sido exitoso");
        alert.showAndWait();
        limpiarCampos();
    }

    private boolean validarCampos(String... campos) {
        for (int i = 0; i < campos.length; i++) {
            if (campos[i] == null || "".equals(campos[i])) {
                return false;
            }
        }
        return true;
    }

    private void limpiarCampos(){
        txtdescripcionpedido.clear();
        lblnumeropedido.setText("N°");
    }

}
