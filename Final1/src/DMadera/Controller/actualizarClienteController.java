package DMadera.Controller;

import DMadera.BSN.clienteBSN;
import DMadera.Modelos.Cliente;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import java.util.List;

public class actualizarClienteController {
    @FXML
    private ComboBox cmbclientes;
    @FXML
    private TextField txtnombrecliente;
    @FXML
    private TextField txtidcliente;
    @FXML
    private TextField txtcontactocliente;

    private clienteBSN clientebsn = new clienteBSN();

    public void initialize(){
        List<Cliente> clientes = clientebsn.listarCliente();
        ObservableList<Cliente> clientesExistentes = FXCollections.observableList(clientes);
        this.cmbclientes.setItems(clientesExistentes);
    }

    public void cmbclientes_Action(){
        Cliente clienteElegido = (Cliente)cmbclientes.getValue();
        if(clienteElegido == null){
            return;
        }
        this.txtnombrecliente.setText(clienteElegido.getNombre());
        this.txtidcliente.setText(clienteElegido.getId());
        this.txtcontactocliente.setText(clienteElegido.getContacto());
    }

    public void btnborrarcliente_Action(){
        Cliente clienteElegido = (Cliente)cmbclientes.getValue();
        if(clienteElegido == null){
            return;
        }
        clientebsn.borrarCliente(clienteElegido);
    }

    public void btnguardarcliente_Action(){
        Cliente clienteElegido = (Cliente)cmbclientes.getValue();
        if (clienteElegido == null) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Actualizacion de cliente");
            alert.setHeaderText("Ha ocurrido un error");
            alert.setContentText("Seleccione un cliente");
            alert.showAndWait();
            return;
        }

        String nombreIngresado = txtnombrecliente.getText().trim();
        String idIngresado = txtidcliente.getText().trim();
        String contactoIngresado = txtcontactocliente.getText().trim();

        boolean esValido = validarCampos(nombreIngresado,idIngresado,contactoIngresado);

        if(!esValido){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Actalizacion de cliente");
            alert.setHeaderText("Ha ocurrido un error");
            alert.setContentText("Diligencie todos los campos");
            alert.showAndWait();
            return;
        }

        clientebsn.actualizarCliente(clienteElegido,nombreIngresado,idIngresado,contactoIngresado);
        limpiarCampos();
    }

    private boolean validarCampos(String... campos) {
        for (int i = 0; i < campos.length; i++) {
            if (campos[i] == null || "".equals(campos[i])) {
                return false;
            }
        }
        return true;
    }

    public void limpiarCampos(){
        txtnombrecliente.clear();
        txtidcliente.clear();
        txtcontactocliente.clear();
    }

}
