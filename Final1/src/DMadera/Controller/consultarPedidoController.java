package DMadera.Controller;

import DMadera.BSN.pedidoBSN;
import DMadera.Modelos.Pedido;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;

import java.util.List;

public class consultarPedidoController {
    @FXML
    private TableView<Pedido> tblpedidos;
    @FXML
    private TableColumn<Pedido,String> clmnumeropedido;
    @FXML
    private TableColumn<Pedido,String> clmestado;
    @FXML
    private TableColumn<Pedido,String> clmcliente;
    @FXML
    private TableColumn<Pedido,String> clmnovedad;
    @FXML
    private TableColumn<Pedido,String> clmdescripcion;
    @FXML
    private ComboBox cmbpedidos;

    private pedidoBSN pedidobsn = new pedidoBSN();

    @FXML
    private void initialize(){
        List<Pedido> pedidosExistentes = pedidobsn.listarPedido();
        ObservableList<Pedido> pedidos = FXCollections.observableList(pedidosExistentes);
        this.cmbpedidos.setItems(pedidos);

        clmcliente.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getCliente().getNombre()));
        clmnumeropedido.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getId()));
        clmestado.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getEstado()));
        clmnovedad.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getNovedad()));
        clmdescripcion.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().getDescripcionNovedad()));
    }

    public void cmbpedidos_Action(){
        Pedido pedidoElegido = (Pedido) cmbpedidos.getValue();
        if(pedidoElegido == null){
            return;
        }
        List<Pedido> pedido = this.pedidobsn.consultarPedidos(pedidoElegido.getIdcliente());
        ObservableList<Pedido> pedidoObservable = FXCollections.observableList(pedido);
        tblpedidos.setItems(pedidoObservable);
    }

    }


