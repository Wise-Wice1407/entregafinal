package DMadera.Controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class CtrprincipalController {
    @FXML
    private BorderPane ctrlprincipal;

    public void mnuconsultarpedido_Action(){
        AnchorPane consultarPedido = null;
        try {
            consultarPedido = FXMLLoader.load (getClass().getResource("../View/consultarPedido.fxml"));
            this.ctrlprincipal.setCenter(consultarPedido);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void mnuregistrarpedido_Action(){
        AnchorPane registrarPedido = null;
        try {
            registrarPedido = FXMLLoader.load(getClass().getResource("../View/registrarPedido.fxml"));
            this.ctrlprincipal.setCenter(registrarPedido);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void mnuregistrarcliente_Action(){
        AnchorPane registroCliente = null;
        try {
            registroCliente = FXMLLoader.load(getClass().getResource("../View/registrarCliente.fxml"));
            this.ctrlprincipal.setCenter(registroCliente);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void mnuactualizarpedido_Action(){
        AnchorPane actualizarPedido = null;
        try {
            actualizarPedido = FXMLLoader.load (getClass().getResource("../View/actualizarPedido.fxml"));
            this.ctrlprincipal.setCenter(actualizarPedido);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void mnucatalogo_Action(){
        AnchorPane catologo = null;
        try {
            catologo = FXMLLoader.load (getClass().getResource("../View/catalogo.fxml"));
            this.ctrlprincipal.setCenter(catologo);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void mnuactualizarcliente_Action(){
        AnchorPane actualizarCliente = null;
        try {
            actualizarCliente = FXMLLoader.load (getClass().getResource("../View/actualizarCliente.fxml"));
            this.ctrlprincipal.setCenter(actualizarCliente);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
