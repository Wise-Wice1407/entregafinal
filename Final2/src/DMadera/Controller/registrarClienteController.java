package DMadera.Controller;

import DMadera.BSN.clienteBSN;
import DMadera.DAO.Exception.LlaveDuplicadaException;
import DMadera.Modelos.Cliente;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;


public class registrarClienteController {
    @FXML
    private TextField txtnombres;
    @FXML
    private TextField txtid;
    @FXML
    private TextField txtcontacto;

    private clienteBSN clientebsn = new clienteBSN();

    @FXML
    public void initialize() {
        txtid.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlNewText().length() <= 12) {
                return change;
            }
            return null;
        }));
    }

    public void btnconfirmar_Action(){
        String nombreIngresado = txtnombres.getText().trim();
        String idIngresado = txtid.getText().trim();
        String contactoIngresado = txtcontacto.getText().trim();

        boolean esValido = validarCampos(nombreIngresado,idIngresado,contactoIngresado);

        if(!esValido){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de cliente");
            alert.setHeaderText("Ha ocurrido un error");
            alert.setContentText("Diligencie todos los campos");
            alert.showAndWait();
            return;
        }

        Cliente cliente = new Cliente(nombreIngresado,idIngresado,contactoIngresado);
        try {
            clientebsn.registrarCliente(cliente);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Registro de cliente");
            alert.setHeaderText("Resultado de la operación");
            alert.setContentText("El registro ha sido exitoso");
            alert.showAndWait();
            limpiarCampos();
        } catch (LlaveDuplicadaException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Registro de cliente");
            alert.setHeaderText("Ha ocurrido un error");
            alert.setContentText("Un cliente con esa identificación ya existe");
            alert.showAndWait();
        }

    }

    private boolean validarCampos(String... campos) {
        for (int i = 0; i < campos.length; i++) {
            if (campos[i] == null || "".equals(campos[i])) {
                return false;
            }
        }
        return true;
    }

    private void limpiarCampos(){
        txtid.clear();
        txtnombres.clear();
        txtcontacto.clear();
    }
}
