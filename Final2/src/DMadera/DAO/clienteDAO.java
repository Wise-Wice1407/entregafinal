package DMadera.DAO;


import DMadera.DAO.Exception.LlaveDuplicadaException;
import DMadera.Modelos.Cliente;

import java.util.List;

public interface clienteDAO {

    void registrarCliente(Cliente cliente) throws LlaveDuplicadaException;

    List<Cliente> consultarCliente(String id);

    List<Cliente> listarCliente();

    void borrarCliente(Cliente clienteElegido);

    void actualizarCliente(Cliente cliente, String nombre, String id, String contacto);

    void agregarProducto(Cliente cliente);

    String pedidosCliente(Cliente cliente);
}
