package DMadera.DAO;

import DMadera.DAO.Exception.LlaveDuplicadaException;
import DMadera.Modelos.Pedido;

import java.util.List;

public interface pedidoDAO {
    void registrarPedido(Pedido pedido) throws LlaveDuplicadaException;

    void borrarPedido(Pedido pedido);

    List<Pedido> consultarPedidos(String id);

    List<Pedido> listarPedido();

    void actualizarPedido(Pedido pedido, String estado, String novedad, String descripcion);
}
