package DMadera.DAO.Exception;

public class LlaveDuplicadaException extends Exception{
    public LlaveDuplicadaException(String llave) {
        super(String.format("Ya existe un registro con esa identificación: %s", llave));
    }
}
