package DMadera.DAO.Impl;

import DMadera.DAO.Exception.LlaveDuplicadaException;
import DMadera.DAO.pedidoDAO;
import DMadera.Modelos.Pedido;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static java.nio.file.StandardOpenOption.APPEND;

public class pedidoDAONio implements pedidoDAO {
    private final static String NOMBRE_ARCHIVO = "pedidos";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static String FIELD_SEPARATOR = "-";
    private final static String RECORD_SEPARATOR = System.lineSeparator();

    public pedidoDAONio() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarPedido(Pedido pedido) throws LlaveDuplicadaException{
        String pedidoString = parsePedido2String(pedido);
        byte[] datosRegistro = pedidoString.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
            fileChannel.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private String parsePedido2String(Pedido pedido) {
        StringBuilder sb = new StringBuilder();
        sb.append(pedido.getId()).append(FIELD_SEPARATOR)
                .append(pedido.getDescripcion()).append(FIELD_SEPARATOR)
                .append(pedido.getEstado()).append(FIELD_SEPARATOR)
                .append(pedido.getNovedad()).append(FIELD_SEPARATOR)
                .append(pedido.getDescripcionNovedad()).append(FIELD_SEPARATOR)
                .append(pedido.getIdcliente()).append(RECORD_SEPARATOR);
        return sb.toString();
    }

    @Override
    public List<Pedido> listarPedido() {
        List<Pedido> pedidos = new ArrayList<>();
        try(Stream<String> stream = Files.lines(ARCHIVO)){
            stream.forEach(pedidoString -> pedidos.add(parsePedido2Object(pedidoString)));
        }catch (IOException ioe){
            ioe.printStackTrace();
        }

        return pedidos;
    }

    @Override
    public void actualizarPedido(Pedido pedido, String estado, String novedad, String descripcion) {
        List<Pedido> pedidos = new ArrayList<>();
        try(Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(pedidoString -> pedidos.add(parsePedido2Object(pedidoString)));
            borrarArchivo(ARCHIVO);
            if (!Files.exists(ARCHIVO)) {
                try {
                    Files.createFile(ARCHIVO);
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                for (int i = 0; i < pedidos.size(); i++) {
                    if (pedidos.get(i) == pedido) {
                        Pedido actualizarPedido = new Pedido(pedido.getId(),pedido.getDescripcion());
                        actualizarPedido.setEstado(estado);
                        actualizarPedido.setNovedad(novedad);
                        actualizarPedido.setDescripcionNovedad(descripcion);
                        String pedidoString = parsePedido2String(actualizarPedido);
                        byte[] datosRegistro = pedidoString.getBytes();
                        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
                        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
                            fileChannel.write(byteBuffer);
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    } else {
                        String clienteString = parsePedido2String(pedidos.get(i));
                        byte[] datosRegistro = clienteString.getBytes();
                        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
                        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
                            fileChannel.write(byteBuffer);
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    }
                }
            }else{
                System.out.println("No se ha podido crear el archivo nuevos");
            }

        }catch (IOException e){

        }
    }

    @Override
    public void borrarPedido(Pedido pedido) {
        List<Pedido> pedidos = new ArrayList<>();
        try(Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(pedidoString -> pedidos.add(parsePedido2Object(pedidoString)));
            borrarArchivo(ARCHIVO);
            if (!Files.exists(ARCHIVO)) {
                try {
                    Files.createFile(ARCHIVO);
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                for (int i = 0; i <= pedidos.size(); i++) {
                    if (pedidos.get(i) == pedido) {
                        continue;
                    } else {
                        String clienteString = parsePedido2String(pedidos.get(i));
                        byte[] datosRegistro = clienteString.getBytes();
                        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
                        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
                            fileChannel.write(byteBuffer);
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    }
                }
            }else{
                System.out.println("No se ha podido crear el archivo nuevos");
            }

        }catch (IOException e){

        }
    }

    @Override
    public List<Pedido> consultarPedidos(String id) {
        List<Pedido> pedidos = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            pedidos = stream.filter(pedidoString -> pedidoString.contains(id))
                    .map(this::parsePedido2Object)
                    .collect(Collectors.toList());
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return pedidos;
    }

    private Pedido parsePedido2Object(String pedidoString) {
        String[] datosPedido = pedidoString.split(FIELD_SEPARATOR);
        Pedido pedido = new Pedido(datosPedido[0],
                datosPedido[1]);
        return pedido;
    }

    private void borrarArchivo(Path archivo){
        try {
            if(Files.exists(archivo)){
                Files.delete(archivo);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
