package DMadera.DAO.Impl;

import DMadera.DAO.Exception.LlaveDuplicadaException;
import DMadera.DAO.clienteDAO;
import DMadera.Modelos.Cliente;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.file.StandardOpenOption.APPEND;

public class clienteDAONio implements clienteDAO {
    private final static String NOMBRE_ARCHIVO = "clientes";
    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);
    private final static String FIELD_SEPARATOR = "-";
    private final static String RECORD_SEPARATOR = System.lineSeparator();

    public clienteDAONio(){
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public void registrarCliente(Cliente cliente) throws LlaveDuplicadaException{
        List<Cliente> clienteOptional = consultarCliente(cliente.getId());
        if(!clienteOptional.isEmpty()){
            throw new LlaveDuplicadaException(cliente.getId());
        }
        clienteOptional = consultarCliente(cliente.getContacto());
        if(!clienteOptional.isEmpty()){
            throw new LlaveDuplicadaException(cliente.getContacto());
        }
        String clienteString = parseCliente2String(cliente);
        byte[] datosRegistro = clienteString.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
            fileChannel.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private String parseCliente2String(Cliente cliente) {
        StringBuilder sb = new StringBuilder();
        sb.append(cliente.getNombre()).append(FIELD_SEPARATOR)
                .append(cliente.getId()).append(FIELD_SEPARATOR)
                .append(cliente.getContacto()).append(FIELD_SEPARATOR)
                .append(cliente.getPedidos()).append(RECORD_SEPARATOR);
        return sb.toString();
    }

    @Override
    public List<Cliente> listarCliente() {
        List<Cliente> clientes = new ArrayList<>();
        try (Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(clienteString -> clientes.add(parseCliente2Object(clienteString)));
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return clientes;
    }

    private Cliente parseCliente2Object(String clienteString) {
        String[] datosCliente = clienteString.split(FIELD_SEPARATOR);
        Cliente cliente = new Cliente(datosCliente[0],
                datosCliente[1], datosCliente[2]);
        return cliente;
    }

    @Override
    public List<Cliente> consultarCliente(String id) {
        List<Cliente> cliente = new ArrayList<>();
        try(Stream<String> stream = Files.lines(ARCHIVO)){
            cliente = stream.filter(clienteString -> clienteString.contains(id))
                    .map(this::parseCliente2Object)
                    .collect(Collectors.toList());
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
        return cliente;
    }

    @Override
    public void borrarCliente(Cliente cliente) {
        List<Cliente> clientes = new ArrayList<>();
        try(Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(clienteString -> clientes.add(parseCliente2Object(clienteString)));
            borrarArchivo(ARCHIVO);
            if (!Files.exists(ARCHIVO)) {
                try {
                    Files.createFile(ARCHIVO);
                } catch (IOException ioe) {
                    System.out.println("Ya existe un archivo en ese nombre");
                }
                for (int i = 0; i < clientes.size(); i++) {
                    if (clientes.get(i) == cliente) {
                        continue;
                    } else {
                        String clienteString = parseCliente2String(clientes.get(i));
                        byte[] datosRegistro = clienteString.getBytes();
                        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
                        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
                            fileChannel.write(byteBuffer);
                        } catch (IOException ioe) {
                            System.out.println("No se logro la escritura en el archivo");
                        }
                    }
                }
            }else{
                System.out.println("No se ha podido crear el archivo nuevos");
            }

        }catch (IOException e){

        }

    }

    @Override
    public void actualizarCliente(Cliente cliente, String nombre, String id, String contacto) {
        List<Cliente> clientes = new ArrayList<>();
        try(Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(clienteString -> clientes.add(parseCliente2Object(clienteString)));
            borrarArchivo(ARCHIVO);
            if (!Files.exists(ARCHIVO)) {
                try {
                    Files.createFile(ARCHIVO);
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
            for (int i = 0; i < clientes.size(); i++) {
                if (clientes.get(i) == cliente) {
                    Cliente actualizarCliente = new Cliente(nombre, id, contacto);
                    String clienteString = parseCliente2String(actualizarCliente);
                    byte[] datosRegistro = clienteString.getBytes();
                    ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
                    try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
                        fileChannel.write(byteBuffer);
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                } else {
                    String clienteString = parseCliente2String(clientes.get(i));
                    byte[] datosRegistro = clienteString.getBytes();
                    ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
                    try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
                        fileChannel.write(byteBuffer);
                    } catch (IOException ioe) {
                        ioe.printStackTrace();
                    }
                }
            }
        }else{
                System.out.println("No se ha podido crear el archivo nuevos");
            }

        }catch (IOException e){

        }
    }

    @Override
    public void agregarProducto(Cliente cliente) {
        List<Cliente> clientes = new ArrayList<>();
        try(Stream<String> stream = Files.lines(ARCHIVO)) {
            stream.forEach(clienteString -> clientes.add(parseCliente2Object(clienteString)));
            borrarArchivo(ARCHIVO);
            if (!Files.exists(ARCHIVO)) {
                try {
                    Files.createFile(ARCHIVO);
                } catch (IOException ioe) {
                    ioe.printStackTrace();
                }
                for (int i = 0; i < clientes.size(); i++) {
                    if (clientes.get(i) == cliente) {
                        Cliente actualizarCliente = new Cliente(cliente.getNombre(),cliente.getId(),cliente.getContacto());
                        cliente.setPedidos(Integer.parseInt(pedidosCliente(cliente))+1);
                        String clienteString = parseCliente2String(actualizarCliente);
                        byte[] datosRegistro = clienteString.getBytes();
                        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
                        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
                            fileChannel.write(byteBuffer);
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    } else {
                        String clienteString = parseCliente2String(clientes.get(i));
                        byte[] datosRegistro = clienteString.getBytes();
                        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
                        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
                            fileChannel.write(byteBuffer);
                        } catch (IOException ioe) {
                            ioe.printStackTrace();
                        }
                    }
                }
            }else{
                System.out.println("No se ha podido crear el archivo nuevos");
            }

        }catch (IOException e){

        }

    }

    @Override
    public String pedidosCliente(Cliente cliente){
        List<String> buscar = new ArrayList<>();
        try(Stream<String> stream = Files.lines(ARCHIVO)){
            buscar = stream.filter(clienteString -> clienteString.contains(cliente.getId())).collect(Collectors.toList());
        }catch (IOException e){

        }
        String[] datos = buscar.get(buscar.size()-1).split(FIELD_SEPARATOR);
        return datos[3];
    }

    private void borrarArchivo(Path archivo){
        try {
            if(Files.exists(archivo)){
                Files.delete(archivo);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
