package DMadera.BSN;

import DMadera.DAO.Exception.LlaveDuplicadaException;
import DMadera.DAO.Impl.clienteDAONio;
import DMadera.DAO.Impl.pedidoDAONio;
import DMadera.DAO.clienteDAO;
import DMadera.DAO.pedidoDAO;
import DMadera.Modelos.Cliente;
import DMadera.Modelos.Pedido;
import java.util.List;

public class clienteBSN {
    private clienteDAO clientedao;
    private pedidoDAO pedidodao;

    public clienteBSN(){
        this.clientedao = new clienteDAONio();
        this.pedidodao = new pedidoDAONio();
    }

    public void registrarCliente(Cliente cliente) throws LlaveDuplicadaException {
        this.clientedao.registrarCliente(cliente);
    }

    public void registrarPedido(Pedido pedido, Cliente cliente) throws LlaveDuplicadaException {
        this.pedidodao.registrarPedido(pedido);
        this.clientedao.agregarProducto(cliente);
    }

    public List<Cliente> listarCliente() {
        return this.clientedao.listarCliente();
    }

    public void borrarCliente(Cliente cliente) {
        this.clientedao.borrarCliente(cliente);
    }

    public void actualizarCliente(Cliente cliente, String nombre, String id, String contacto) {
        this.clientedao.actualizarCliente(cliente,nombre,id,contacto);
    }

    public String pedidosCliente(Cliente cliente) {
        return clientedao.pedidosCliente(cliente);
    }
}
