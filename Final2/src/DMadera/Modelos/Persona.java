package DMadera.Modelos;

public abstract class Persona {
    private String Nombre,Id;

    public Persona(String nombre, String id) {
        Nombre = nombre;
        Id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "Nombre='" + Nombre + '\'' +
                ", Id='" + Id + '\'' +
                '}';
    }
}
