package DMadera.Modelos;

public class Pedido {
    private String Id,Descripcion,Estado,novedad,descripcionNovedad;

    private Cliente cliente;
    private String idcliente;

    public Pedido(String id, String descripcion) {
        Id = id;
        Descripcion = descripcion;
        Estado = "Confirmando";
        novedad = "Ninguna";
        descripcionNovedad = "";
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String descripcion) {
        Descripcion = descripcion;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getIdcliente() {
        return idcliente;
    }

    public void setIdcliente(String idcliente) {
        this.idcliente = idcliente;
    }

    public String getNovedad() {
        return novedad;
    }

    public void setNovedad(String novedad) {
        this.novedad = novedad;
    }

    public String getDescripcionNovedad() {
        return descripcionNovedad;
    }

    public void setDescripcionNovedad(String descripcionNovedad) {
        this.descripcionNovedad = descripcionNovedad;
    }

    @Override
    public String toString() {
        return "Pedido{" +
                "Id='" + Id + '\'' +
                ", Descripcion='" + Descripcion + '\'' +
                '}';
    }
}
