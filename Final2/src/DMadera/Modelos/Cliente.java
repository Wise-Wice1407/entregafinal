package DMadera.Modelos;

import java.util.ArrayList;
import java.util.List;

public class Cliente extends Persona{
    private String descripcionPedido,contacto;

    private int pedidos;

    public Cliente(String nombre,String id,String contacto) {
        super(nombre,id);
        this.contacto = contacto;
        pedidos = 0;
    }

    public int getPedidos() {
        return pedidos;
    }

    public void setPedidos(int pedidos) {
        this.pedidos = pedidos;
    }

    public String getDescripcionPedido() {
        return descripcionPedido;
    }

    public void setDescripcionPedido(String descripcionPedido) {
        this.descripcionPedido = descripcionPedido;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public String getId(){
        return super.getId();
    }

    public void setId(String id){
        super.setId(id);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
